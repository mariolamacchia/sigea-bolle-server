const sequelize = require('../src/sequelize');
const User = require('../src/auth/User.js');

sequelize.sync({ force: true }).then(() => {
  console.log('starting sync');
  return User.create({
    id: 0,
    password: 'password',
    isAdmin: true,
    isFirstAccess: true,
  });
}).then(() => {
  console.log('done');
  process.exit(0);
}, (error) => {
  console.error(error);
  process.exit(1);
});
