const jwt = require('./jwt');
const User = require('./User');

const generateAuthMiddleware = fn => (req, res, next) => {
  const token = req.headers.authorization;
  const { userId } = jwt.verifyToken(token);
  User.findById(userId, { raw: true }).then((user) => {
    if (!user) {
      res.sendStatus(401);
    }

    delete user.password;
    req.user = user;
    fn(req, res, next);
  });
};

exports.loggedUnsafe = generateAuthMiddleware((req, res, next) => next());

exports.logged = generateAuthMiddleware((req, res, next) => {
  if (req.user.isFirstAccess) {
    res.setHeader('Location', '/change-password');
    res.sendStatus(403);
  } else {
    next();
  }
});

exports.adminOnly = generateAuthMiddleware((req, res, next) => {
  if (!req.user.isAdmin || req.user.isFirstAccess) {
    res.sendStatus(403);
  } else {
    next();
  }
});
