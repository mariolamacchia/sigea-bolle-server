const crypto = require('crypto');

function isValidPassword(password) {
  return password && password.length >= 6;
}

function getRandomPassword() {
  return crypto.randomBytes(4).toString('hex');
}

module.exports = {
  isValidPassword,
  getRandomPassword,
};
