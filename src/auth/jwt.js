'use strict';

const jwt = require('jsonwebtoken');
const config = require('../config');

const generateToken = (userId) => {
  return jwt.sign({ userId }, config.secret, { expiresIn: config.jwsExpiration });
};

const verifyToken = (token) => {
  return jwt.verify(token, config.secret);
}

module.exports = { generateToken, verifyToken };
