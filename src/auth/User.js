const bcrypt = require('bcrypt');
const Sequelize = require('sequelize');
const sequelize = require('../sequelize');
const config = require('../config');

const User = sequelize.define('user', {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    allowNull: false,
  },
  password: {
    type: Sequelize.STRING,
    allowNull: false,
    set(password) {
      console.log(password);
      this.setDataValue('password', bcrypt.hashSync(password, config.saltRounds));
    },
  },
  isAdmin: { type: Sequelize.BOOLEAN, allowNull: false },
  isFirstAccess: { type: Sequelize.BOOLEAN, allowNull: false },
});


User.findUsers = () => User.findAll({
  attributes: {
    exclude: ['password'],
  },
  where: {
    isAdmin: false,
  },
});

User.checkCredentials = (id, password) => {
  console.log(password);
  return User.findById(id).then((user) => {
    if (!(user && bcrypt.compareSync(password, user.password))) {
      throw new Error('Invalid credentials');
    }
    return user;
  });
};

User.changePassword = (id, oldPassword, newPassword) => {
  return User.checkCredentials(id, oldPassword).then((user) => {
    user.password = newPassword;
    user.isFirstAccess = false;
    return user.save();
  });
};

module.exports = User;
