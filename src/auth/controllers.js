const router = require('express').Router();
const auth = require('./auth');
const jwt = require('./jwt');
const User = require('./User');
const { loggedUnsafe, adminOnly } = require('./middlewares');

router.post('/login', (req, res) => {
  const { id, password } = req.body;
  User.checkCredentials(id, password).then(() => {
    const token = jwt.generateToken(id);
    res.json({ token });
  }, () => {
    res.sendStatus(401);
  });
});

router.put('/users/:id', adminOnly, (req, res) => {
  const { id } = req.params;
  const { newPassword, oldPassword } = req.body;
  if (!auth.isValidPassword(newPassword)) {
    res.sendStatus(422);
  }

  User.changePassword(id, oldPassword, newPassword).then(() => {
    res.sendStatus(200);
  }, () => {
    res.sendStatus(401);
  });
});

router.get('/me', loggedUnsafe, (req, res) => {
  res.json(req.user);
});

router.get('/users', adminOnly, (req, res) => {
  User.findUsers().then((users) => {
    res.json(users);
  });
});

router.delete('/users/:id', adminOnly, (req, res) => {
  const { id } = req.params;
  User.destroy({
    where: { id },
  }).then(
    () => res.sendStatus(200),
    () => res.sendStatus(500)
  );
});

router.post('/users', adminOnly, (req, res) => {
  const { id } = req.body;
  console.log(id, req.body);
  const password = auth.getRandomPassword();

  User.create({
    id,
    password,
    isAdmin: false,
    isFirstAccess: true,
  }).then(() => {
    res.json({ password });
  }, (error) => {
    res.status(500).send(error.errors);
  });
});

router.post('/users/:id/reset', adminOnly, (req, res) => {
  const { id } = req.params;
  const password = auth.getRandomPassword();
  User.findById(id).then((user) => {
    if (!user) {
      res.sendStatus(404);
    }
    user.password = password;
    user.isFirstAccess = true;
    return user.save();
  }).then(() => {
    res.json({ password });
  }, () => {
    res.sendStatus(500);
  });
});

module.exports = router;
