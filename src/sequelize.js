const Sequelize = require('sequelize');
const config = require('./config');

console.log(`Connecting to db: ${config.sequelize.replace(/:[^/]*@/, ':******@')}`);

module.exports = new Sequelize(config.sequelize, {
  operatorsAliases: false,
});
