'use strict';

const env = process.env.ENV || 'DEV';
module.exports = require(`./${env.toLowerCase()}`);
