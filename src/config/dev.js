'use strict';

const dbHost = process.env.DB_HOST || 'localhost';
const dbUser = process.env.DB_USER || 'root';
const dbPassword = process.env.DB_PASSWORD || '';
const dbDatabase = process.env.DB_DATABASE || 'sigeabolledev';

exports.secret = process.env.JWT_SECRET || 'secret';
exports.jwsExpiration = '1y';
exports.sequelize = `mysql://${dbUser}:${dbPassword}@${dbHost}/${dbDatabase}`;
exports.saltRounds = 10;
exports.port = process.env.PORT || 3000;
