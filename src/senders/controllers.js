const router = require('express').Router();
const { logged } = require('../auth/middlewares');

router.use(logged);
router.get('/', (req, res) => {
  res.send([{ id: 1 }, { id: 2 }]);
});

module.exports = router;
