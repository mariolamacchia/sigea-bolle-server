module.exports = {
  extends: 'airbnb-base',
  rules: {
    'no-param-reassign': 'off',
    'comma-dangle': ['error', 'only-multiline', {
      functions: 'never'
    }],
    'no-console': 'off',
  },
};
